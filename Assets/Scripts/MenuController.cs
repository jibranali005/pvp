﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public void OnClickPicker (int selectedCharacter)
    {
        if (PlayerInfo.info != null)
        {
            PlayerInfo.info.mySelectedCharacter = selectedCharacter;
            PlayerPrefs.SetInt ("MyCharacter", selectedCharacter);
        }

    }
}
