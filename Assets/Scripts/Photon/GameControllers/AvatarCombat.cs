﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class AvatarCombat : MonoBehaviour
{
    private PhotonView view;
    private AvatarSetup setup;
    public Transform rayOrigin;
    void Start()
    {
        view = GetComponent<PhotonView>();
        setup = GetComponent<AvatarSetup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!view.IsMine)
        return;

        if (Input.GetMouseButtonDown(0))
            view.RPC("RPC_Shooting", RpcTarget.All);

       
    }

    [PunRPC]
    void RPC_Shooting ()
    {
         RaycastHit hit;

        if (Physics.Raycast(rayOrigin.position, rayOrigin.TransformDirection(Vector3.forward), out hit, 1000))
        {
            Debug.DrawRay(rayOrigin.position, rayOrigin.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            Debug.Log("hit");
            if (hit.transform.tag == "Avatar")
            {
                hit.transform.gameObject.GetComponent<AvatarSetup>().playerHealth -= setup.playerDamage;
            }
        }
        else
        {
            Debug.DrawRay(rayOrigin.position, rayOrigin.TransformDirection(Vector3.forward) * hit.distance, Color.white);
            Debug.Log("did not hit");
        }
    }
}
