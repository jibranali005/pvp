﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class AvatarSetup : MonoBehaviour
{
    private PhotonView view;
    public int characterValue;
    public int playerHealth;
    public int playerDamage;
    public GameObject myCharacter; 
    public Camera myCamera;
    public AudioListener myListener ;

    void Start()
    {
        view = GetComponent <PhotonView>();
        if (view.IsMine)
            view.RPC("RPC_AddCharacter", RpcTarget.AllBuffered, PlayerInfo.info.mySelectedCharacter);
        else
        {
            Destroy (myCamera);
            Destroy (myListener);
        }
    }

    [PunRPC]
    void RPC_AddCharacter(int selectedCharacter)
    {
        characterValue = selectedCharacter;
        myCharacter = Instantiate (PlayerInfo.info.allCharacters[selectedCharacter],transform.position,transform.rotation,transform);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
