﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerController : MonoBehaviour
{
    private PhotonView view;
    private CharacterController controller;
    public float movementSpeed;
    public float rotationSpeed;

    void Start()
    {
        view = GetComponent<PhotonView>();
        controller = GetComponent <CharacterController>();   
    }

    // Update is called once per frame
    void Update()
    {
        if (view.IsMine)
        {
            BasicMovement();
            BasicRotation();
        }
        
    }

    void BasicMovement ()
    {
        if (Input.GetKey(KeyCode.W))
            controller.Move(transform.forward * Time.deltaTime * movementSpeed);
        else if (Input.GetKey(KeyCode.A))
            controller.Move(-transform.right * Time.deltaTime * movementSpeed);
        else if (Input.GetKey(KeyCode.S))
            controller.Move(-transform.forward * Time.deltaTime * movementSpeed);
        else if (Input.GetKey(KeyCode.D))
            controller.Move(transform.right * Time.deltaTime * movementSpeed);

    }

    void BasicRotation ()
    {
        float mouseX = Input.GetAxis("Mouse X") * Time.deltaTime * rotationSpeed ;
        transform.Rotate(new Vector3 (0, mouseX, 0));
    }
}
