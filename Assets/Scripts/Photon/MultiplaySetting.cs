﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplaySetting : MonoBehaviour
{
    public static MultiplaySetting settings;

    public bool delayStart;
    public int maxPlayers;
    public int menuScene;
    public int multiplayerScene;

    private void Awake ()
    {
        if (MultiplaySetting.settings == null)
            MultiplaySetting.settings = this;
        else if (MultiplaySetting.settings != this)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad (this.gameObject);
            

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
