﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class PunLobby : MonoBehaviourPunCallbacks
{
    public static PunLobby lobby;
    RoomInfo[] rooms;
    public GameObject battleButton;
    public GameObject cancelButton;

    private void Awake ()
    {

    lobby = this;

    }

    // Start is called before the first frame update
    void Start()
    {
     PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster ()
    {
        Debug.Log (" Connected to Master Server ");
        PhotonNetwork.AutomaticallySyncScene = true;
        battleButton.SetActive(true);
    }

    public void OnBattleClicked()
    {
        battleButton.SetActive(false);
        cancelButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log ("Join Failed. No Open Rooms");
        CreateRoom ();
    }

    void CreateRoom ()
    {
        int randomRoom =  Random.Range (0,10000);
        RoomOptions ops = new RoomOptions (){ IsVisible = true, IsOpen = true, MaxPlayers = (byte)MultiplaySetting.settings.maxPlayers};
        PhotonNetwork.CreateRoom ("Room" + randomRoom, ops);
    
    }

    public override void OnCreatedRoom()
    {
        Debug.Log ("Room Created");
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log (" Create Room failed, Room with same name exists ");
        CreateRoom();
    }


    public void OnCancelClicked ()
    { 
        cancelButton.SetActive(false);
        battleButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
