﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PunRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
   
   public static PunRoom room;
   private PhotonView view;

   public bool isGameLoaded;
   public int currentScene;

   Player[] players;
   public int playersInRoom;
   public int myNoInRoom;

   public int playerInGame;

   // DelayStart

   private bool readyToCount;
   private bool readyToStart;
   public float startingTime;
   private float lessThanMaxPlayers;
   private float atMaxPlayer;
   private float timeToStart;


    private void Awake ()
    {
        if (PunRoom.room == null)
            PunRoom.room = this;
        else if (PunRoom.room != this)
        {
            Destroy (PunRoom.room.gameObject);
            PunRoom.room = this ;
        }

        DontDestroyOnLoad (this.gameObject);
        
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceneFinishedLoading; 
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }


    void Start()
    {
        view = GetComponent<PhotonView>();
        readyToCount = false;
        readyToStart = false;
        lessThanMaxPlayers = startingTime;
        atMaxPlayer = 6;
        timeToStart = startingTime;
    }

    public override void OnJoinedRoom()
    {
     base.OnJoinedRoom();
     Debug.Log ("Room Joined");
     players = PhotonNetwork.PlayerList;
     playersInRoom = players.Length;
     myNoInRoom = playersInRoom;
     PhotonNetwork.NickName = myNoInRoom.ToString();
     if (MultiplaySetting.settings.delayStart)
     {
        Debug.Log ("Displayer players in room out of max possible ( " + playersInRoom+" | "+MultiplaySetting.settings.maxPlayers+" )");

        if (playersInRoom>1)
            readyToCount = true;

        if (playersInRoom ==  MultiplaySetting.settings.maxPlayers)
        {
            readyToStart = true;
            if (!PhotonNetwork.IsMasterClient)
                return;
            PhotonNetwork.CurrentRoom.IsOpen = false;

        }
        

     }
     else
        StartGame();

    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
     base.OnPlayerEnteredRoom(newPlayer);
     Debug.Log ("Room Joined by new player");
     players = PhotonNetwork.PlayerList;
     playersInRoom++;
    
     if (MultiplaySetting.settings.delayStart)
     {
        Debug.Log ("Displayer players in room out of max possible ( " + playersInRoom+" | "+MultiplaySetting.settings.maxPlayers+" )");

        if (playersInRoom>1)
            readyToCount = true;

        if (playersInRoom ==  MultiplaySetting.settings.maxPlayers)
        {
            readyToStart = true;
            if (!PhotonNetwork.IsMasterClient)
                return;
            PhotonNetwork.CurrentRoom.IsOpen = false;

        }
        
     }
     
    }
    // Update is called once per frame
    void Update()
    {
        if (MultiplaySetting.settings.delayStart)
        {
            if (playersInRoom == 1)
                RestartTimer();

            if (!isGameLoaded)
            {
                if (readyToStart)
                {
                    atMaxPlayer -= Time.deltaTime ;
                    lessThanMaxPlayers = atMaxPlayer;
                    timeToStart = atMaxPlayer;

                }
                else if (readyToCount)
                {
                    lessThanMaxPlayers -= Time.deltaTime;
                    timeToStart = lessThanMaxPlayers ;
                }
                Debug.Log("Display Time to Start" + timeToStart);
                if (timeToStart<=0)
                    StartGame();
            }    
            
        }
        
    }

    void StartGame()
    {
        isGameLoaded = true;
        if (!PhotonNetwork.IsMasterClient)
            return;
        if  (MultiplaySetting.settings.delayStart)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
        }
        PhotonNetwork.LoadLevel(MultiplaySetting.settings.multiplayerScene);
    }

    void RestartTimer()
    {
        lessThanMaxPlayers = startingTime;
        timeToStart = startingTime;
        atMaxPlayer = 6 ;
        readyToCount = false;
        readyToStart = false;
    }

    void OnSceneFinishedLoading (Scene scene, LoadSceneMode mode)
    {
        currentScene = scene.buildIndex;

        if (currentScene == MultiplaySetting.settings.multiplayerScene)
        {
            isGameLoaded = true;

            if (MultiplaySetting.settings.delayStart)
            {
                view.RPC ("RPC_LoadedGameScene", RpcTarget.MasterClient);
            }
            else
            {
                RPC_CreatePlayer();
            }


        }
    }

    [PunRPC]
    private void RPC_LoadedGameScene()
    {
        playerInGame++;
        if (playerInGame == PhotonNetwork.PlayerList.Length)
        {
            view.RPC("RPC_CreatePlayer",RpcTarget.All);
        }
    }

    [PunRPC]
    private void RPC_CreatePlayer()
    {
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonNetworkPlayer"), transform.position, Quaternion.identity, 0);
    }

}
